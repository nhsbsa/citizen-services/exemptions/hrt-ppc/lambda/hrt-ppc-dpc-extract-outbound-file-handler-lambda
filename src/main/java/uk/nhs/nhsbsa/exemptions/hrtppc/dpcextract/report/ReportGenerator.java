package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.report;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.model.PharmacyReportItem;

/** Generates DPC reports. */
public class ReportGenerator {
  private static final DateTimeFormatter REPORT_DATE_FORMAT =
      DateTimeFormatter.ofPattern("yyyy-MM-dd");

  /** Generates a pharmacy costs report for DPC. */
  public String generateReport(LocalDate startDate, List<PharmacyReportItem> reportItems) {
    return "CertificateType,OCSCode,PaymentDate,PaymentAmount\n"
        + reportItems.stream()
            .map(
                item ->
                    String.format(
                        "%s,%s,%s,%s",
                        item.type(),
                        item.pharmacyId(),
                        startDate.format(REPORT_DATE_FORMAT),
                        item.totalCost()))
            .collect(Collectors.joining("\n"));
  }
}
