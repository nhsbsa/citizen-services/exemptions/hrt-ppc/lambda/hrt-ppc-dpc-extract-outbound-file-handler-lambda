package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception;

/** Thrown when there's an error encrypting. */
public class EncryptionException extends RuntimeException {
  public EncryptionException(String message, Throwable cause) {
    super(message, cause);
  }
}
