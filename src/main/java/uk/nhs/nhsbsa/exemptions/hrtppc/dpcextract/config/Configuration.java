package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config;

/** The local configuration of the system. */
public class Configuration {
  private static final String DPC_EXTRACT_BUCKET_NAME = System.getenv("DPC_EXTRACT_BUCKET_NAME");
  private static final String AWS_REGION = System.getenv("AWS_REGION");
  private static final String AWS_SECRETS_EXTENSION_HTTP_HOST =
      System.getenv("AWS_SECRETS_EXTENSION_HTTP_HOST");
  private static final String AWS_SECRETS_EXTENSION_HTTP_PORT =
      System.getenv("AWS_SECRETS_EXTENSION_HTTP_PORT");
  private static final String AWS_SESSION_TOKEN = System.getenv("AWS_SESSION_TOKEN");
  private static final String DPC_EXTRACT_ENABLE_ASCII_ARMOR =
      System.getenv("DPC_EXTRACT_ENABLE_ASCII_ARMOR");
  private static final String CERTIFICATE_API_BASE_URL_SM_ID =
      System.getenv("CERTIFICATE_API_BASE_URL_SM_ID");
  private static final String CERTIFICATE_API_KEY_SM_ID =
      System.getenv("CERTIFICATE_API_KEY_SM_ID");
  private static final String DPC_EXTRACT_ENCRYPTION_ALGORITHM_SM_ID =
      System.getenv("DPC_EXTRACT_ENCRYPTION_ALGORITHM_SM_ID");
  private static final String DPC_EXTRACT_PUBLIC_KEY_SM_ID =
      System.getenv("DPC_EXTRACT_PUBLIC_KEY_SM_ID");
  private static final String DPC_EXTRACT_BUCKET_ENDPOINT =
      System.getenv("DPC_EXTRACT_BUCKET_ENDPOINT");

  public String getDpcExtractBucketEndpoint() {
    return DPC_EXTRACT_BUCKET_ENDPOINT;
  }

  public String getDpcExtractBucketName() {
    return DPC_EXTRACT_BUCKET_NAME;
  }

  public String getAwsRegion() {
    return AWS_REGION;
  }

  public String getAwsSecretsExtensionHttpHost() {
    return AWS_SECRETS_EXTENSION_HTTP_HOST;
  }

  public String getAwsSecretsExtensionHttpPort() {
    return AWS_SECRETS_EXTENSION_HTTP_PORT;
  }

  public String getAwsSessionToken() {
    return AWS_SESSION_TOKEN;
  }

  public String getCertificateApiBaseUrlSecretId() {
    return CERTIFICATE_API_BASE_URL_SM_ID;
  }

  public String getCertificatesApiKeySecretId() {
    return CERTIFICATE_API_KEY_SM_ID;
  }

  public String getDpcExtractEncryptionAlgorithmSecretId() {
    return DPC_EXTRACT_ENCRYPTION_ALGORITHM_SM_ID;
  }

  public String getDpcExtractPublicKeySecretId() {
    return DPC_EXTRACT_PUBLIC_KEY_SM_ID;
  }

  public String getDpcExtractEnableAsciiArmor() {
    return DPC_EXTRACT_ENABLE_ASCII_ARMOR;
  }
}
