package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception.PharmacyDataException;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.model.PharmacyReportItem;

/** A client to communicate with a Certificates API. */
public class CertificatesApiClient {
  private static final String URI_CERTIFICATES_SUFFIX = "/v1/certificates/group-by-pharmacy";
  private static final String X_API_KEY_HEADER = "x-api-key";
  private static final String USER_ID_HEADER = "user-id";
  private static final String USER_ID_VALUE = "DPC_BATCH_FILE_OUT";
  private static final String CHANNEL_HEADER = "channel";
  private static final String BATCH_CHANNEL = "BATCH";
  private static final String CORRELATION_ID_HEADER = "correlation-id";

  private static final String CERTIFICATE_TYPE_PARAM = "certificateType";
  private static final String HRT_PPC_TYPE = "HRT_PPC";
  private static final String STATUS_PARAM = "status";
  private static final String ACTIVE_STATUS = "ACTIVE";
  private static final String APPLICATION_START_DATE_PARAM = "applicationStartDate";
  private static final String APPLICATION_END_DATE_PARAM = "applicationEndDate";
  private static final String SIZE_PARAM = "size";
  private static final String PAGE_PARAM = "page";
  private static final String SORT_PARAM = "sort";
  private static final String SORT_VALUE = "pharmacyId,ASC";

  private final RestTemplateBuilder restTemplateBuilder;
  private final Logger logger;
  private final String endpointUri;
  private final String apiKey;

  /** A client with injected factory for rest templates, and configuration. */
  public CertificatesApiClient(
      RestTemplateBuilder restTemplateBuilder, Logger logger, String endpointUri, String apiKey) {
    this.restTemplateBuilder = restTemplateBuilder;
    this.logger = logger;
    this.endpointUri = endpointUri;
    this.apiKey = apiKey;
  }

  /**
   * Retrieves total costs of active HRT PPC's per pharmacy from between the {@code startDate} and
   * {@code endDate}.
   */
  public List<PharmacyReportItem> retrievePharmacyReportData(
      LocalDate startDate, LocalDate endDate) {
    int page = 0;
    List<PharmacyReportItem> results = new ArrayList<>();
    boolean needMorePages;
    do {
      ResponseEntity<String> response = findPharmacyReportDataPage(startDate, endDate, page);
      var responseJson = new JSONObject(interpretResponse(response));

      JSONArray reportItems =
          responseJson.getJSONObject("_embedded").getJSONArray("pharmacyReportItems");

      IntStream.range(0, reportItems.length())
          .mapToObj(reportItems::getJSONObject)
          .map(this::extractPharmacyReportItem)
          .forEach(results::add);

      JSONObject pageJson = responseJson.getJSONObject("page");
      int currentPageNumber = pageJson.getInt("number");
      int totalPages = pageJson.getInt("totalPages");

      needMorePages = currentPageNumber < totalPages - 1;
      page++;
    } while (needMorePages);
    return results;
  }

  private ResponseEntity<String> findPharmacyReportDataPage(
      LocalDate startDate, LocalDate endDate, int page) {
    RestTemplate template = restTemplateBuilder.build();
    URI uri =
        UriComponentsBuilder.fromUriString(endpointUri + URI_CERTIFICATES_SUFFIX)
            .queryParam(CERTIFICATE_TYPE_PARAM, HRT_PPC_TYPE)
            .queryParam(STATUS_PARAM, ACTIVE_STATUS)
            .queryParam(APPLICATION_START_DATE_PARAM, startDate)
            .queryParam(APPLICATION_END_DATE_PARAM, endDate)
            .queryParam(PAGE_PARAM, page)
            .queryParam(SIZE_PARAM, 1000)
            .queryParam(SORT_PARAM, SORT_VALUE)
            .build()
            .toUri();
    RequestEntity<Void> requestEntity =
        RequestEntity.get(uri)
            .header(X_API_KEY_HEADER, apiKey)
            .header(USER_ID_HEADER, USER_ID_VALUE)
            .header(CHANNEL_HEADER, BATCH_CHANNEL)
            .header(CORRELATION_ID_HEADER, UUID.randomUUID().toString())
            .build();

    return template.exchange(requestEntity, String.class);
  }

  private PharmacyReportItem extractPharmacyReportItem(JSONObject json) {
    try {
      logger.info("Parsing report item for pharmacy = {}", json.optString("pharmacyId"));
      if (json.getLong("totalCost") < 0) {
        throw new IllegalArgumentException("Total cost cannot be negative");
      } else {
        return new PharmacyReportItem(
            json.getString("pharmacyId"),
            json.getString("type"),
            json.getString("status"),
            Math.negateExact(json.getLong("totalCost")));
      }
    } catch (JSONException e) {
      throw new PharmacyDataException("Unable to process pharmacy data", e);
    }
  }

  private <T> T interpretResponse(ResponseEntity<T> response) {
    if (response.getStatusCode().is2xxSuccessful()) {
      return response.getBody();
    }
    throw new RestClientException("Unsuccessful response: " + response.getStatusCode().value());
  }
}
