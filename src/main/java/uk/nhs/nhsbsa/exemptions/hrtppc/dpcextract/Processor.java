package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract;

import com.amazonaws.SdkClientException;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.springframework.web.client.RestClientException;
import uk.nhs.nhsbsa.encryption.pgp.PgpEncryptionUtil;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client.CertificatesApiClient;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client.DpcOutboundClient;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception.EncryptionException;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception.PharmacyDataException;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception.ReportUploadException;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.model.PharmacyReportItem;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.report.ReportGenerator;

/** Processes an event to collect, format and upload a pharmacy costs report. */
public class Processor {
  private final Clock clock;
  private final CertificatesApiClient certificatesClient;
  private final ReportGenerator reportGenerator;
  private final PgpEncryptionUtil encryptor;
  private final DpcOutboundClient outboundClient;
  private final Logger logger;

  /** Constructs a processor with all dependencies. */
  public Processor(
      Clock clock,
      Logger logger,
      CertificatesApiClient certificatesClient,
      ReportGenerator reportGenerator,
      PgpEncryptionUtil encryptor,
      DpcOutboundClient outboundClient) {
    this.clock = clock;
    this.logger = logger;
    this.certificatesClient = certificatesClient;
    this.reportGenerator = reportGenerator;
    this.encryptor = encryptor;
    this.outboundClient = outboundClient;
  }

  /** Processes an event to collect, format, encrypt and upload a pharmacy costs report. */
  public void process() {
    LocalDate startDate = LocalDate.now(clock).minusMonths(2).withDayOfMonth(1);
    LocalDate endDate = startDate.withDayOfMonth(startDate.lengthOfMonth());

    List<PharmacyReportItem> reportItems = requestPharmacyData(startDate, endDate);
    String plaintext = compileOutput(startDate, reportItems);
    String ciphertext = encrypt(plaintext);
    uploadReport(ciphertext);
  }

  private String encrypt(String plaintext) {
    try {
      return encryptor.encrypt(plaintext);
    } catch (PGPException | IOException ex) {
      throw new EncryptionException("Error while encrypting DPC report", ex);
    }
  }

  private List<PharmacyReportItem> requestPharmacyData(LocalDate startDate, LocalDate endDate) {
    try {
      return certificatesClient.retrievePharmacyReportData(startDate, endDate);
    } catch (RestClientException ex) {
      throw new PharmacyDataException("Unable to retrieve pharmacy data", ex);
    }
  }

  private String compileOutput(LocalDate startDate, List<PharmacyReportItem> reportItems) {
    reportItems.forEach(item -> logger.info("Compiling pharmacy record: [{}]", item));
    return reportGenerator.generateReport(startDate, reportItems);
  }

  private void uploadReport(String output) {
    String bucketKey =
        String.format(
            "outbound/payment/completed/hrtppc_pharmacy_payment_%s.csv.pgp",
            LocalDate.now(clock).format(DateTimeFormatter.ofPattern("yyyyMM")));
    try {
      outboundClient.publishDpcReport(output, bucketKey);
    } catch (SdkClientException e) {
      throw new ReportUploadException("Unable to upload pharmacy report", e);
    }
  }
}
