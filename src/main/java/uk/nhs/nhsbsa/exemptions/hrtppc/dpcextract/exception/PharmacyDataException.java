package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception;

/** Thrown when there's a problem retrieving or processing the pharmacy data. */
public class PharmacyDataException extends RuntimeException {
  public PharmacyDataException(String message, Throwable cause) {
    super(message, cause);
  }
}
