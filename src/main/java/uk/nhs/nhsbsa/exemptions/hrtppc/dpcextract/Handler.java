package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.springframework.boot.web.client.RestTemplateBuilder;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config.Configuration;

/**
 * Handles events. No relevant data is expected to be included in the event as the event is just a
 * trigger to start processing.
 */
public class Handler implements RequestHandler<Void, Void> {
  private ProcessorFactory factory =
      new ProcessorFactory(new Configuration(), new RestTemplateBuilder());

  public void setProcessorFactory(ProcessorFactory factory) {
    this.factory = factory;
  }

  @Override
  public Void handleRequest(Void input, Context context) {
    factory.createProcessor().process();
    return null;
  }
}
