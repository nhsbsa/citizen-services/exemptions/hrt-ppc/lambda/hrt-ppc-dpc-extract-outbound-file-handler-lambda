package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import java.time.Clock;
import java.time.ZoneId;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import uk.nhs.nhsbsa.encryption.pgp.PgpEncryptionUtil;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client.CertificatesApiClient;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client.DpcOutboundClient;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config.Configuration;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config.SecretsCache;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception.EncryptionException;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.report.ReportGenerator;

/** Creates a {@link Processor}. */
public class ProcessorFactory {
  private final Configuration config;
  private final RestTemplateBuilder restTemplateBuilder;

  public ProcessorFactory(Configuration config, RestTemplateBuilder restTemplateBuilder) {
    this.config = config;
    this.restTemplateBuilder = restTemplateBuilder;
  }

  /** Creates a fully wired processor. */
  public Processor createProcessor() {
    final var secretsCache = createSecretsCache();
    final var certificatesClient = createCertificatesClient(secretsCache);
    final var outboundClient = createOutboundClient();
    final var reportGenerator = new ReportGenerator();
    final var encryptor = createEncryptor(secretsCache);

    return new Processor(
        Clock.system(ZoneId.of("Europe/London")),
        LoggerFactory.getLogger(Processor.class),
        certificatesClient,
        reportGenerator,
        encryptor,
        outboundClient);
  }

  private SecretsCache createSecretsCache() {
    return new SecretsCache(
        restTemplateBuilder,
        config.getAwsSecretsExtensionHttpHost(),
        Integer.parseInt(config.getAwsSecretsExtensionHttpPort()),
        config.getAwsSessionToken());
  }

  private DpcOutboundClient createOutboundClient() {
    final AwsClientBuilder.EndpointConfiguration endpoint =
        new AwsClientBuilder.EndpointConfiguration(
            config.getDpcExtractBucketEndpoint(), config.getAwsRegion());
    final var s3 =
        AmazonS3ClientBuilder.standard()
            .enablePathStyleAccess()
            .withEndpointConfiguration(endpoint)
            .build();

    return new DpcOutboundClient(
        s3, LoggerFactory.getLogger(DpcOutboundClient.class), config.getDpcExtractBucketName());
  }

  private CertificatesApiClient createCertificatesClient(SecretsCache secretsCache) {
    return new CertificatesApiClient(
        new RestTemplateBuilder(),
        LoggerFactory.getLogger(CertificatesApiClient.class),
        secretsCache.getSecretValue(config.getCertificateApiBaseUrlSecretId()),
        secretsCache.getSecretValue(config.getCertificatesApiKeySecretId()));
  }

  private PgpEncryptionUtil createEncryptor(SecretsCache secretsCache) {
    try {
      return new PgpEncryptionUtil(
          secretsCache.getSecretValue(config.getDpcExtractEncryptionAlgorithmSecretId()),
          secretsCache.getSecretValue(config.getDpcExtractPublicKeySecretId()),
          Boolean.parseBoolean(config.getDpcExtractEnableAsciiArmor()));
    } catch (Exception e) {
      throw new EncryptionException("Failed to create encryptor", e);
    }
  }
}
