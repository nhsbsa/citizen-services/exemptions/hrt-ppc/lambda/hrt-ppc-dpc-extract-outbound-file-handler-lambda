package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client;

import com.amazonaws.services.s3.AmazonS3;
import org.slf4j.Logger;

/** A client to communicate with the DPC outbound messaging channel. */
public class DpcOutboundClient {
  private final AmazonS3 s3;
  private final String bucketName;
  private final Logger logger;

  /** Client for communicating with the DPC outbound channel. */
  public DpcOutboundClient(AmazonS3 s3, Logger logger, String bucketName) {
    this.s3 = s3;
    this.logger = logger;
    this.bucketName = bucketName;
  }

  /** Publishes a report to DPC. */
  public void publishDpcReport(String outboundFileContents, String fileName) {
    s3.putObject(bucketName, fileName, outboundFileContents);
    logger.info("File uploaded: {}", fileName);
  }
}
