package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.model;

/** A single item in the pharmacy report extract. */
public record PharmacyReportItem(String pharmacyId, String type, String status, Long totalCost) {}
