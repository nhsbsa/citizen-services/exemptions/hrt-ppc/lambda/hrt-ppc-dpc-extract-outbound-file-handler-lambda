package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.exception;

/** Thrown when there's a problem uploading the report data. */
public class ReportUploadException extends RuntimeException {
  public ReportUploadException(String message, Throwable cause) {
    super(message, cause);
  }
}
