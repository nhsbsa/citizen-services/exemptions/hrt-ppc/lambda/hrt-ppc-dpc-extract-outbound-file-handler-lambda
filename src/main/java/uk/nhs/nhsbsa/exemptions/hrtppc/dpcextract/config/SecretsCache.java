package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config;

import java.net.URI;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/** A cache of secrets. */
public class SecretsCache {
  private final RestTemplateBuilder templateBuilder;
  private final String awsSecretsExtensionHttpHost;
  private final int awsSecretsExtensionHttpPort;
  private final String awsSessionToken;

  /** Constructs a new cache with required properties and dependencies. */
  public SecretsCache(
      RestTemplateBuilder templateBuilder,
      String awsSecretsExtensionHttpHost,
      int awsSecretsExtensionHttpPort,
      String awsSessionToken) {
    this.templateBuilder = templateBuilder;
    this.awsSecretsExtensionHttpHost = awsSecretsExtensionHttpHost;
    this.awsSecretsExtensionHttpPort = awsSecretsExtensionHttpPort;
    this.awsSessionToken = awsSessionToken;
  }

  /** Retrieves a secret from the cache. */
  public String getSecretValue(@NonNull String secretName) {
    URI uri =
        UriComponentsBuilder.fromUriString(
                String.format(
                    "http://%s:%d/secretsmanager/get",
                    awsSecretsExtensionHttpHost, awsSecretsExtensionHttpPort))
            .queryParam("secretId", secretName)
            .build()
            .toUri();
    RequestEntity<Void> req =
        RequestEntity.get(uri).header("X-AWS-Parameters-Secrets-Token", awsSessionToken).build();
    RestTemplate restTemplate = templateBuilder.build();
    ResponseEntity<String> res = restTemplate.exchange(req, String.class);
    String body = res.getBody();
    JSONObject json = new JSONObject(body);
    return json.getString("SecretString");
  }
}
