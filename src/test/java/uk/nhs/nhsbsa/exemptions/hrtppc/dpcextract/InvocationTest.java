package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.matches;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.mockito.stubbing.OngoingStubbing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import uk.nhs.nhsbsa.encryption.pgp.PgpEncryptionUtil;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client.CertificatesApiClient;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.client.DpcOutboundClient;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.report.ReportGenerator;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class InvocationTest {
  private static final String encryptionAlgorithm = "AES_256";
  private static final String pubKey =
      """
      -----BEGIN PGP PUBLIC KEY BLOCK-----

      mQENBGSUJ1gBCADOl7Sj6Ncw5JKTa7DJAg8qzekbwx5BDN89giNQCs3MNYic06AU
      jn2hSI0k/hXp0m9smWZnoK7JjffTSj7gNlIJFXY2alSkvqI+SnGVLC09H0323k11
      8fEAjZPm1QtdiHt5XzSVSVdENasBwEWxIXhG2SHjl/Mto6V65L+HBfxDzRYVsmen
      vF7alW8sjhWcA/NJdlZZsbY7rkxN4TCSEBjWLrRb9rmnacL17JuTP4CpXOgT9T4c
      yNlrEEq6ghjnPdGMITUZyshyzE3wKG7K2CxeV99BfOqGfhU1Yi8ZAel0NSW0PbfE
      1Leyq+tJx6IkMZzkpWdO/2Wl+YhJuCjj8wnDABEBAAG0GEJpbiBZdSA8YmluLnl1
      QHRlc3QuY29tPokBUQQTAQgAOxYhBLtd91+Ur620tyF6/CZDjvK5DuIGBQJklCdY
      AhsDBQsJCAcCAiICBhUKCQgLAgQWAgMBAh4HAheAAAoJECZDjvK5DuIGoLAIALXm
      3JLARRCH+pP1VF0gP0FwbVD8GNTn5+AWSkoRrdjrQ/WccsizJ8ab1R6peb2b4WcQ
      OvzPmMxyCEz/9eyQ37mnB5zenA90Sc/0q9VLDJP3aakmjYsTWJoX+NBGBr/2oKHr
      DdEFkBeJHKzBI4xiXmJKVRaCRg1wPWDF89PpDWWgmCi3c7uHvvUNouMccmw/jIn3
      D53p6XxgwlXtmVg0POSO3+Kl1zAoSClJWPIIUvia3G13EOk5ODuRaUw82ZvX7uPS
      YlY+btlaElCgnmbvhMgMTd0ku2Nj4mEOuAxnuhOAadLmMT6tAwHKMPwPZz/T3PQF
      5gbKnTkuKgiC/L9ESu65AQ0EZJQnWAEIAMiXsOOnE0MG/GQwJJcDGe6vLVzTUf/j
      lsFBLDuiC9x2KAcFIXnS3ZF8kpsGG1MhKT+Pml9jSi0Ov53FBsKSDg2cke0RayTN
      Rvdx+5jg+Mcv7GSz334O1g4oQ8ygz9x0rfdc2w7ZcgRDKNHHwOLJIEeSxNKpO2nZ
      2TCakK/Ta1ijm7jxrBLAdlQIJ35iPqSg/aWLgAvYMRGBCloIqQB6ekGQYUBh8z0c
      NoTN484suRPZTxcUCUxpSJ0e94SLdyXllU6a+49SUYmpdBmLlDehhi3QaCTpjQYz
      Bme5l3azXQjYf7dinzmP3c2NGt2Q2L8FlqMW3D80hvfUv8o/Np+ux0EAEQEAAYkB
      NgQYAQgAIBYhBLtd91+Ur620tyF6/CZDjvK5DuIGBQJklCdYAhsMAAoJECZDjvK5
      DuIG810IAMO6DLZFLNkp3HTfyBs7hepuEcsj1U4MRLRWdGAJ8vRWhq+ZiVhepisN
      kid/RBq/xp9LgFvs/o8aqmFbLwD0+hlZgwqAtZAzzZ2yMTmprzwsBoAbMubT0ShN
      /YXwzs1AC4wNPPCE3YPEA3qKABdwnC7G0JLBKqzBF9QMiQCcK7HHb82pOekSGOdh
      sLGQBevdd2r3qfzViVF5D9eTm8UUY3mmi/nVR0NcWSIEuJ8GLS8qsTmcFV0bNPMN
      4ny2Z68XTK5sLtRfqX+zHKcnXOdeBOMCh4c++yOJKj1J7Zg5Dq0CJvc4vyCzFeQU
      2QZ/kZeH+P9FcaA+86fl5gkuUcs/TCU=
      =gFKf
      -----END PGP PUBLIC KEY BLOCK-----
      """;
  private static final String MOCK_ENDPOINT = "mock-endpoint";
  private final Clock fixedClock =
      Clock.fixed(Instant.parse("2023-06-15T12:34:56.000Z"), ZoneId.of("Europe/London"));
  @Mock private RestTemplateBuilder mockRestTemplateBuilder;
  @Mock private RestTemplate mockRestTemplate;
  @Mock private ResponseEntity<String> mockResponse;
  @Mock private AmazonS3 mockAmazonS3;
  @Mock private Logger mockLogger;
  @Mock private ProcessorFactory factory;
  private PgpEncryptionUtil encryptorSpy;
  private Handler handler;

  private String encryptionResult;

  @BeforeEach
  void setUp() throws Exception {
    when(mockRestTemplateBuilder.build()).thenReturn(mockRestTemplate);
    when(mockRestTemplate.exchange(any(), eq(String.class))).thenReturn(mockResponse);
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.OK);

    setUpWithClock(fixedClock);
  }

  @Test
  void shouldStopWhenUnableToRetrievePharmacyData() {
    whenUnableToRetrievePharmacyData(new RestClientException("expected exception"));
    catchThrowable(() -> handler.handleRequest(null, null));
    shouldNeverUploadReport();
  }

  @Test
  @Tag("implementation")
  void shouldStopWhenNon2xxResponseCodeReceived() {
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.SWITCHING_PROTOCOLS);
    assertThatThrownBy(() -> handler.handleRequest(null, null))
        .hasMessage("Unable to retrieve pharmacy data")
        .hasRootCauseMessage("Unsuccessful response: 101");
  }

  @Test
  void shouldThrowWhenUnableToRetrievePharmacyData() {
    var expectedException = new RestClientException("expected exception");
    whenUnableToRetrievePharmacyData(expectedException);
    assertThatThrownBy(() -> handler.handleRequest(null, null))
        .hasMessage("Unable to retrieve pharmacy data")
        .hasCause(expectedException);
  }

  @Test
  void shouldThrowWhenUnableToInterpretAnyPharmacyData() {
    whenPharmacyDataIs(
        "{\"pharmacyId\":\"fake-pharmacy-id-1\",\"type\":\"HRT_PPC\",\"status\":\"ACTIVE\",\"totalCost\":1234}",
        // missing pharmacyId
        "{\"type\":\"HRT_PPC_TEST\",\"status\":\"ACTIVE\",\"totalCost\":5678}",
        "{\"pharmacyId\":\"fake-pharmacy-id-3\",\"type\":\"HRT_PPC\",\"status\":\"ACTIVE\",\"totalCost\":9012}");

    assertThatThrownBy(() -> handler.handleRequest(null, null))
        .hasMessage("Unable to process pharmacy data");
    shouldNeverUploadReport();
  }

  @Test
  void shouldThrowWhenUnableToUploadReport() {
    whenPharmacyDataIs("");
    var expectedException = new SdkClientException("expected exception");
    whenUnableToUploadReport(expectedException);
    assertThatThrownBy(() -> handler.handleRequest(null, null))
        .hasMessage("Unable to upload pharmacy report")
        .hasCause(expectedException);
  }

  @Test
  void shouldThrowWhenPaymentAmountNegativeBeforeReport() {
    whenPharmacyDataIs(
        """
        {
          "pharmacyId": "fake-pharmacy-id",
          "type": "HRT_PPC",
          "status": "ACTIVE",
          "totalCost": -1234
        }""");

    assertThatThrownBy(() -> handler.handleRequest(null, null))
        .hasMessage("Total cost cannot be negative");
  }

  @Test
  void shouldCreateBlankFileWhenNoPharmacyData() throws Exception {
    whenPharmacyDataIs("");
    handler.handleRequest(null, null);
    uploadedReportShouldMatch("CertificateType,OCSCode,PaymentDate,PaymentAmount\n");
  }

  @Test
  void shouldCreateSingleRowFileWhenOneReportItemIsRetrieved() throws Exception {
    whenPharmacyDataIs(
        """
        {
          "pharmacyId": "fake-pharmacy-id",
          "type": "HRT_PPC",
          "status": "ACTIVE",
          "totalCost": 1234
        }""");

    handler.handleRequest(null, null);

    uploadedReportShouldMatch(
        """
        CertificateType,OCSCode,PaymentDate,PaymentAmount
        HRT_PPC,fake-pharmacy-id,[^,]+,-1234""");
  }

  @ParameterizedTest(name = "when execution date is {0} payment date should be {1}")
  @CsvSource({
    "2023-08-15T00:00:00Z,2023-06-01",
    "2023-12-15T23:59:59Z,2023-10-01",
    "2024-01-03T12:00:00Z,2023-11-01"
  })
  @DisplayName("")
  void shouldCalculateDateTwoMonthsPrior(String executionDate, String paymentDate)
      throws Exception {
    whenTheTimeIs(executionDate);
    whenPharmacyDataIs(
        """
        {
          "pharmacyId": "fake-pharmacy-id",
          "type": "HRT_PPC",
          "status": "ACTIVE",
          "totalCost": 1234
        }""");

    handler.handleRequest(null, null);

    uploadedReportShouldMatch(
        String.format(
            """
            CertificateType,OCSCode,PaymentDate,PaymentAmount
            HRT_PPC,fake-pharmacy-id,%s,-1234""",
            paymentDate));
  }

  @Test
  void shouldUploadReportWithAllPharmacyData() throws Exception {
    whenTheTimeIs("2023-01-23T12:34:56Z");
    whenPharmacyDataIs(
        """
        {
          "pharmacyId": "fake-pharmacy-id-1",
          "type": "HRT_PPC",
          "status": "ACTIVE",
          "totalCost": 1234
        }""",
        """
        {
          "pharmacyId": "fake-pharmacy-id-2",
          "type": "HRT_PPC_TEST",
          "status": "ACTIVE",
          "totalCost": 5678
        }""");

    handler.handleRequest(null, null);

    uploadedReportShouldMatch(
        """
        CertificateType,OCSCode,PaymentDate,PaymentAmount
        HRT_PPC,fake-pharmacy-id-1,2022-11-01,-1234
        HRT_PPC_TEST,fake-pharmacy-id-2,2022-11-01,-5678""");
  }

  @Test
  @Tag("implementation")
  void shouldRequestPharmacyDataInCorrectRange() throws Exception {
    whenTheTimeIs("2023-09-03T11:00:00Z");
    whenPharmacyDataIs("");

    handler.handleRequest(null, null);

    var captor = ArgumentCaptor.forClass(RequestEntity.class);
    verify(mockRestTemplate).exchange(captor.capture(), eq(String.class));
    RequestEntity<?> req = captor.getValue();
    assertThat(req.getUrl().toString())
        .matches(
            MOCK_ENDPOINT
                + "/v1/certificates/group-by-pharmacy\\?"
                + "(?=.*\\bcertificateType=HRT_PPC(?:&|$))"
                + "(?=.*\\bstatus=ACTIVE(?:&|$))"
                + "(?=.*\\bapplicationStartDate=2023-07-01(?:&|$))"
                + "(?=.*\\bapplicationEndDate=2023-07-31(?:&|$))"
                + "(?=.*\\bpage=0(?:&|$))"
                + "(?=.*\\bsize=1000(?:&|$))"
                + "(?=.*\\bsort=pharmacyId,ASC(?:&|$))"
                + ".*");
  }

  @Test
  @Tag("implementation")
  void shouldRequestPharmacyDataWithCorrectHeaders() {
    whenPharmacyDataIs("");

    handler.handleRequest(null, null);

    var captor = ArgumentCaptor.forClass(RequestEntity.class);
    verify(mockRestTemplate).exchange(captor.capture(), eq(String.class));
    RequestEntity<?> req = captor.getValue();
    var headers = req.getHeaders();
    assertAll(
        () -> assertThat(headers.get("x-api-key")).containsExactly("mock-key"),
        () -> assertThat(headers.get("user-id")).containsExactly("DPC_BATCH_FILE_OUT"),
        () -> assertThat(headers.get("channel")).containsExactly("BATCH"),
        () ->
            assertThat(headers.get("correlation-id"))
                .satisfies(
                    correlationIds -> {
                      assertThat(correlationIds).hasSize(1);
                      assertThat(correlationIds).first().isInstanceOf(String.class);
                    }));
  }

  @Test
  @Tag("implementation")
  void shouldRetrieveAllPagesOfReportItems() {
    whenPharmacyDataIs(
        IntStream.range(0, 2500)
            .mapToObj(
                i ->
                    String.format(
                        """
                        {
                          "pharmacyId": "P%04d",
                          "type": "HRT_PPC",
                          "status": "ACTIVE",
                          "totalCost": 1234
                        }""",
                        i))
            .toList()
            .toArray(new String[2500]));

    handler.handleRequest(null, null);

    var captor = ArgumentCaptor.forClass(RequestEntity.class);
    verify(mockRestTemplate, times(3)).exchange(captor.capture(), eq(String.class));
    List<RequestEntity> reqs = captor.getAllValues();
    verifyRequestWasMadeForPage(reqs, 0);
    verifyRequestWasMadeForPage(reqs, 1);
    verifyRequestWasMadeForPage(reqs, 2);
  }

  private void whenTheTimeIs(String isoDateTime) throws Exception {
    setUpWithClock(Clock.fixed(Instant.parse(isoDateTime), ZoneId.of("Europe/London")));
  }

  private void setUpWithClock(Clock clock) throws Exception {
    var apiClient =
        new CertificatesApiClient(mockRestTemplateBuilder, mockLogger, MOCK_ENDPOINT, "mock-key");
    var outboundClient = new DpcOutboundClient(mockAmazonS3, mockLogger, "mock-bucket");
    var reportGenerator = new ReportGenerator();
    var encryptor = new PgpEncryptionUtil(encryptionAlgorithm, pubKey, true);
    encryptorSpy = Mockito.spy(encryptor);

    encryptionResult = null;
    when(encryptorSpy.encrypt(anyString()))
        .thenAnswer(
            invocation -> {
              var result = invocation.callRealMethod();
              encryptionResult = result.toString();
              return result;
            });

    var processor =
        new Processor(
            clock,
            LoggerFactory.getLogger(Processor.class),
            apiClient,
            reportGenerator,
            encryptorSpy,
            outboundClient);
    when(factory.createProcessor()).thenReturn(processor);

    handler = new Handler();
    handler.setProcessorFactory(factory);
  }

  private void whenUnableToRetrievePharmacyData(RestClientException expectedException) {
    when(mockRestTemplate.exchange(any(RequestEntity.class), any(Class.class)))
        .thenThrow(expectedException);
  }

  private void shouldNeverUploadReport() {
    verify(mockAmazonS3, never()).putObject(anyString(), anyString(), anyString());
  }

  private void whenUnableToUploadReport(SdkClientException expectedException) {
    when(mockAmazonS3.putObject(anyString(), anyString(), anyString()))
        .thenThrow(expectedException);
  }

  private void whenPharmacyDataIs(String... records) {
    final int totalPages = (records.length / 1000) + 1;
    int page = 0;

    OngoingStubbing<String> pageStubbing = when(mockResponse.getBody());
    do {
      int recordsInPage = Math.min(1000, records.length - (1000 * page));
      String[] pageOfRecords =
          Arrays.copyOfRange(records, page * 1000, page * 1000 + recordsInPage);
      String reportItems = String.join(",", pageOfRecords);
      pageStubbing =
          pageStubbing.thenReturn(
              String.format(
                  """
                  {
                    "_embedded": {
                      "pharmacyReportItems": [
                        %s
                      ]
                    },
                    "page": {
                      "number": %d,
                      "totalPages": %d
                    }
                  }""",
                  reportItems, page, totalPages));
      page++;
    } while (page < totalPages);
  }

  private void uploadedReportShouldMatch(String reportRegex) throws Exception {
    verify(encryptorSpy).encrypt(matches(reportRegex));
    verify(mockAmazonS3).putObject(anyString(), anyString(), eq(encryptionResult));
  }

  private void verifyRequestWasMadeForPage(List<RequestEntity> reqs, int page) {
    assertThat(reqs)
        .anySatisfy(
            req ->
                assertThat(req.getUrl().toString())
                    .matches(String.format(".*\\bpage=%d(?:&|$).*", page)));
  }
}
