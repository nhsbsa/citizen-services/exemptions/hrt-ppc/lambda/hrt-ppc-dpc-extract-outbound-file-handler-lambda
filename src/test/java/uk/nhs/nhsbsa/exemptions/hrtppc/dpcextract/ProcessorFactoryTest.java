package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config.Configuration;

@Tag("implementation")
class ProcessorFactoryTest {
  @Test
  void processorFactoryShouldReturnUsableProcessor() {
    // given
    var mockConfig = mock(Configuration.class);
    var mockRestTemplateBuilder = mock(RestTemplateBuilder.class);
    var mockRestTemplate = mock(RestTemplate.class);
    when(mockRestTemplateBuilder.build()).thenReturn(mockRestTemplate);
    when(mockConfig.getAwsRegion()).thenReturn("eu-west-2");
    when(mockConfig.getAwsSecretsExtensionHttpHost()).thenReturn("localhost");
    when(mockConfig.getAwsSecretsExtensionHttpPort()).thenReturn("9999");
    when(mockConfig.getCertificatesApiKeySecretId()).thenReturn("CERTIFICATE_API_KEY");
    when(mockConfig.getCertificateApiBaseUrlSecretId()).thenReturn("CERTIFICATE_API_BASE_URL");
    when(mockConfig.getDpcExtractEncryptionAlgorithmSecretId())
        .thenReturn("DPC_EXTRACT_ENCRYPTION_ALGORITHM");
    when(mockConfig.getDpcExtractPublicKeySecretId()).thenReturn("DPC_EXTRACT_PUBLIC_KEY");
    when(mockConfig.getDpcExtractBucketEndpoint()).thenReturn("localhost");
    when(mockRestTemplate.exchange(any(), eq(String.class)))
        .thenAnswer(ProcessorFactoryTest::getMockedSecretsManagerResponse);
    when(mockConfig.getDpcExtractBucketName()).thenReturn("fake-bucket");
    when(mockConfig.getDpcExtractEnableAsciiArmor()).thenReturn("true");
    var processorFactory = new ProcessorFactory(mockConfig, mockRestTemplateBuilder);

    // when
    var processor = processorFactory.createProcessor();

    // then
    assertThat(processor).isNotNull();
  }

  private static ResponseEntity getMockedSecretsManagerResponse(InvocationOnMock invocation) {
    String secret =
        invocation
            .<RequestEntity<Void>>getArgument(0)
            .getUrl()
            .getQuery() // e.g. "secretId=foobar"
            .split("=")[1];
    String result =
        Map.of(
                "CERTIFICATE_API_KEY", "fake-key",
                "CERTIFICATE_API_BASE_URL", "localhost",
                "DPC_EXTRACT_ENCRYPTION_ALGORITHM", "AES_256",
                "DPC_EXTRACT_PUBLIC_KEY", "FAKE KEY")
            .get(secret);
    var mockResponse = mock(ResponseEntity.class);
    when(mockResponse.getBody())
        .thenReturn(String.format("""
                {"SecretString":"%s"}""", result));
    return mockResponse;
  }
}
