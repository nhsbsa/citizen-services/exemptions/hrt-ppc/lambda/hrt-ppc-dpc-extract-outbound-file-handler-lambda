package uk.nhs.nhsbsa.exemptions.hrtppc.dpcextract.config;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SecretsCacheTest {
  @Mock private RestTemplateBuilder mockRestTemplateBuilder;
  @Mock private RestTemplate mockRestTemplate;
  @Mock private ResponseEntity<String> mockResponse;
  @Captor private ArgumentCaptor<RequestEntity<Void>> requestCaptor;

  private SecretsCache secretsCache;

  @BeforeEach
  void setUp() {
    when(mockRestTemplateBuilder.build()).thenReturn(mockRestTemplate);
    when(mockRestTemplate.exchange(any(), any(Class.class))).thenReturn(mockResponse);
    when(mockResponse.getBody()).thenReturn("""
        {"SecretString":"default"}""");
    secretsCache = new SecretsCache(mockRestTemplateBuilder, "secretshost", 9999, "session-token");
  }

  @Test
  void shouldBubbleErrorWhenMakingRequest() {
    var expectedException = new RestClientException("expected exception");
    when(mockRestTemplate.exchange(any(), any(Class.class))).thenThrow(expectedException);

    assertThatThrownBy(() -> secretsCache.getSecretValue("anything")).isSameAs(expectedException);
  }

  @Test
  void shouldBubbleErrorInterpretingInvalidResponse() {
    when(mockResponse.getBody()).thenReturn("not a valid json string");
    assertThatThrownBy(() -> secretsCache.getSecretValue("anything"));
  }

  @Test
  void shouldReturnSecretStringFromResponse() {
    when(mockResponse.getBody()).thenReturn("""
        {"SecretString":"swordfish"}""");
    String result = secretsCache.getSecretValue("TOP_SECRET_PASSWORD");
    assertThat(result).isEqualTo("swordfish");
  }

  @Test
  void shouldCallCorrectUri() {
    secretsCache.getSecretValue("foobar");

    verify(mockRestTemplate).exchange(requestCaptor.capture(), eq(String.class));
    RequestEntity<Void> request = requestCaptor.getValue();
    assertAll(
        () -> assertThat(request.getUrl().getHost()).isEqualTo("secretshost"),
        () -> assertThat(request.getUrl().getPath()).isEqualTo("/secretsmanager/get"),
        () -> assertThat(request.getUrl().getQuery()).isEqualTo("secretId=foobar"));
  }

  @Test
  void shouldMakeRequestWithCorrectHeader() {
    secretsCache.getSecretValue("irrelevant");

    verify(mockRestTemplate).exchange(requestCaptor.capture(), eq(String.class));
    RequestEntity<Void> request = requestCaptor.getValue();
    assertThat(request.getHeaders())
        .containsOnly(entry("X-AWS-Parameters-Secrets-Token", List.of("session-token")));
  }
}
