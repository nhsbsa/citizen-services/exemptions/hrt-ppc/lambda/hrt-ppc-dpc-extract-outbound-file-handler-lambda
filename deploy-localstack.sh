#!/bin/bash

echo "compiling jar"
./mvnw clean compile package
echo "copying jar to /hrt-ppc-localstack folder"
cp target/dpc-extract-outbound-file-handler-lambda.jar ../hrt-ppc-localstack/lambda-archives
