# DPC extract outbound file handler lambda
A lambda to retrieve pharmacy payments for hrt ppc, generate and upload a file.

## Build
In a shell, navigate to the root folder and use the maven wrapper to build the lambda. 
```bash
./mvnw clean install -U
```

## Deploy
To deploy the application in your localstack

```bash
./deploy-localstack.sh
```

You can use the SAM CLI's local invoke utility:

1. Rename and set ./events/sample-env.json to env.json
2. Run:
```bash
sam local invoke -t ./template.yml -e ./events/event.json --env-vars ./events/env.json
````
